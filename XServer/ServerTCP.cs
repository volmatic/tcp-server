﻿using System;
using System.Net;
using System.Net.Sockets;

namespace XServer
{
    static class ServerTCP
    {
        static TcpListener serverSocket = new TcpListener(IPAddress.Any, 31927);

        public static void InitializeNetwork()
        {
            Console.WriteLine("Initializing packets...");
            PacketHandle.InitializePackets();
            Console.WriteLine("Starting server...");
            serverSocket.Start();
            Console.WriteLine("Starting network...");
            serverSocket.BeginAcceptSocket(new AsyncCallback(OnClientConnect), null);
        }

        private static void OnClientConnect(IAsyncResult ar)
        {
            TcpClient client = serverSocket.EndAcceptTcpClient(ar);
            serverSocket.BeginAcceptSocket(new AsyncCallback(OnClientConnect), null);
            ClientManager.CreateNewConnection(client);
        }
    }
}
