﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace XServer
{
    public class Client
    {
        public int ConnectionID;
        public TcpClient Socket;
        public NetworkStream Stream;
        public ByteBuffer Buffer;

        private byte[] recBuffer;

        public void Start()
        {
            Socket.SendBufferSize = 4096;
            Socket.ReceiveBufferSize = 4096;

            Stream = Socket.GetStream();
            Stream.BeginRead(recBuffer, 0, Socket.ReceiveBufferSize, OnReceiveData, null);
            Console.WriteLine("Incoming connection from '{0}'", Socket.Client.RemoteEndPoint.ToString());
        }

        private void OnReceiveData(IAsyncResult result)
        {
            try
            {
                int length = Stream.EndRead(result);
                if (length <= 0)
                {
                    CloseConnection();
                    return;
                }

                byte[] newBytes = new byte[length];
                Array.Copy(recBuffer, newBytes, length);

                PacketHandle.HandleData(ConnectionID, newBytes); //Handle data
                Stream.BeginRead(recBuffer, 0, Socket.ReceiveBufferSize, OnReceiveData, null); //Start reading again
            }
            catch (Exception)
            {
                CloseConnection();
                throw;
            }
        }

        private void CloseConnection()
        {
            Console.WriteLine("'{0}' connection closed.", Socket.Client.RemoteEndPoint.ToString());
            Socket.Close();
        }
    }
}
