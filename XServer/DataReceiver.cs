﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XServer
{
    public enum ClientPackets
    {
        CHelloServer = 1,
    }

    static class DataReceiver
    {
        public static void HandleHelloServer(int connectionID, byte[] data)
        {
            ByteBuffer buffer = new ByteBuffer();
            buffer.WriteByteArray(data);
            int packetID = buffer.ReadInt();

            string message = buffer.ReadString();
            buffer.Dispose();

            Console.WriteLine(message);
        }
    }
}
